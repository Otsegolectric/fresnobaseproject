#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include "cinder/app/AppBasic.h"
#include "cinder/Surface.h"
#include "cinder/gl/gl.h"
#include "cinder/gl/Texture.h"
#include "cinder/Rand.h"

#include "cinder/ImageIo.h"
#include "cinder/Utilities.h" 
#include "cinder/gl/GlslProg.h"
//#include "cinder/audio/Output.h"
#include <string>

//#include "Emitter.h"
//#include "Resources.h"
#include "cinder/ImageIo.h"
#include "cinder/Timeline.h"
#include "cinder/Vector.h"
#include "cinder/params/Params.h"

using namespace ci;
#include <stdio.h>
#include <list>
#include <Windows.h>
#include <stdio.h>

#pragma unmanaged
class IEffectInterface
	{
	public:
		virtual ~IEffectInterface();
		ci::Anim<ci::Vec3f> Position;
		ci::Vec3f Offset;
		ci::Timeline* privateTimeline;
		int ResetTime;
		int uID;
		virtual void Update();
		virtual	void Draw();
		int effectID;
		virtual	void Initialize(char* InitData, Timeline* CinderTimeline);
		virtual void SetEffectData(char* Data);
		bool isLocked;
		int WindowWidth;
		int WindowHeight;
		int DestinationMapping;
	};


void IEffectInterface::Update(){}
void IEffectInterface::Draw(){}
void IEffectInterface::Initialize(char* InitData, Timeline* CinderTimeline){}
void IEffectInterface::SetEffectData(char* Data){}
IEffectInterface::~IEffectInterface(){}