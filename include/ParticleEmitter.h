#pragma once

#include "Particle.h"
#include <vector>
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "cinder/Timeline.h"

class ParticleEmitter {
public:
	void add(Particle p);
	void setup(cinder::gl::Texture tex);
	void render();
	void update(float deltaTime);
private:
	std::vector<Particle> particles;
	gl::Texture particleTexture;
};